# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render
import datetime
from search.models import CompassDirection, Break

def home(request):
	return render(request, 'index.html', {'current_date':datetime.datetime.now()})

def search(request):
	return render(request, 'search.html', {'all_compass_directions':CompassDirection.objects.all(),'current_date':datetime.datetime.now()})

def do_search(request):
	wind_speed=request.POST["wind_speed"]
	wind_direction=request.POST["wind_direction"]
	matching_breaks=Break.objects.filter(wind_speed__gt=wind_speed)
	matching_breaks=matching_breaks.filter(wind_directions=wind_direction)
	matching_breaks=matching_breaks.order_by('rank')
	return render(request, 'do_search.html', {"matching_breaks":matching_breaks})
