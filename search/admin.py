from django.contrib import admin
from search.models import Break,CompassDirection, Tide


admin.site.register(Break)
admin.site.register(CompassDirection)
admin.site.register(Tide)
