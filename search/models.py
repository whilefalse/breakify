from django.db import models

class CompassDirection(models.Model):
	name=models.CharField(max_length=2)
	def __unicode__(self):
		return self.name

class Tide(models.Model):
	name=models.CharField(max_length=1)
	def __unicode__(self):
		return self.name

class Break(models.Model):
	name=models.CharField(max_length=200)
	swell_size=models.IntegerField()
	swell_period=models.IntegerField()
	wind_speed=models.IntegerField()
	rank=models.IntegerField()
	wind_directions=models.ManyToManyField(CompassDirection, related_name="breaks_from_wind_direction")
	swell_directions=models.ManyToManyField(CompassDirection, related_name="breaks_from_swell_direction")
	tide=models.ManyToManyField(Tide)
	def __unicode__(self):
		return self.name


# Create your models here.
